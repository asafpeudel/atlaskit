import styled from 'styled-components';

const InitializingSpinnerDiv = styled.div`
  text-align: center;
  overflow: hidden;
`;

InitializingSpinnerDiv.displayName = 'InitializingSpinnerDiv';
export default InitializingSpinnerDiv;
